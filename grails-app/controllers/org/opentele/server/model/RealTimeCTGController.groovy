package org.opentele.server.model

import grails.plugins.springsecurity.Secured
import grails.validation.ValidationException
import org.opentele.server.annotations.SecurityWhiteListController
import org.opentele.server.model.types.PermissionName

@Secured(PermissionName.NONE)
@SecurityWhiteListController
class RealTimeCTGController {

    def springSecurityService
    def realTimeCTGService

    static allowedMethods = [save: "POST"]

    @Secured(PermissionName.REALTIME_CTG_SAVE)
    @SecurityWhiteListController
    def save() {
        try {
            realTimeCTGService.save(params)
        } catch (ValidationException ex) {
            response.sendError(400)
            return;
        }

        response.setStatus(201)
        render ''
    }
}
