package org.opentele.taglib

import grails.plugins.springsecurity.SecurityTagLib
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.opentele.server.saml.SamlSpringConfigUtils
import org.springframework.security.saml.SAMLEntryPoint
import org.springframework.security.saml.SAMLLogoutFilter
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate

class SamlTagLib extends SecurityTagLib {

  /**
   * {@inheritDocs}
   */
  def isSamlActive = { attrs, body ->
    if (SamlSpringConfigUtils.isSamlActive) {
      out << body()
    }
  }

  /**
   * {@inheritDocs}
   */
  def isSamlNotActive = { attrs, body ->
    if (!SamlSpringConfigUtils.isSamlActive) {
      out << body()
    }
  }

  /**
   * {@inheritDocs}
   */
  def samlIdpTitle = { attrs, body ->
    if (SamlSpringConfigUtils.isSamlActive)
      out << SpringSecurityUtils.securityConfig.saml.metadata.idp.title ?: body()
    else
      out << body()
  }

  /**
   * {@inheritDocs}
   */
  def loginLink = { attrs, body ->
    def url = "${request.contextPath}${SAMLEntryPoint.FILTER_URL}"
    def elementClass = generateClassAttribute(attrs)
    def elementId = generateIdAttribute(attrs)

    out << "<a href='${url}'${elementId}${elementClass}>${body()}</a>"
  }

  /**
   * {@inheritDocs}
   */
  def logoutLink = { attrs, body ->
    def local = attrs.remove('local')

    def url = (SamlSpringConfigUtils.isSamlActive) ? SAMLLogoutFilter.FILTER_URL : SpringSecurityUtils.securityConfig.logout.filterProcessesUrl
    url = "${request.contextPath}${url}${local?'?local=true':''}"

    def elementClass = generateClassAttribute(attrs)
    def elementId = generateIdAttribute(attrs)

    out << """<a href='${url}'${elementId}${elementClass}>${body()}</a>"""
  }

  /**
   * {@inheritDocs}
   */
  def isReadonlyUserField = { attrs, body ->
    def name = attrs.remove('name')
    if (!canEditUserField(name)) {
      out << body()
    }
  }

  /**
   * {@inheritDocs}
   */
  def isEditableUserField = { attrs, body ->
    def name = attrs.remove('name')
    if (canEditUserField(name)) {
      out << body()
    }
  }

  private boolean canEditUserField(String name) {
    return !SamlSpringConfigUtils.isSamlActive || !SpringSecurityUtils.securityConfig.saml.userAttributes.containsKey(name)
  }

  private String generateIdAttribute(Map attrs) {
    return attrs.id ? " id=\'${attrs.id}\'" : ""
  }

  private String generateClassAttribute(Map attrs) {
    return attrs.class ? " class=\'${attrs.class}\'" : ""
  }
}
