<%@ page import="org.opentele.server.model.Clinician"%>
<%@ page import="org.opentele.server.model.Role"%>

<div
	class="fieldcontain ${hasErrors(bean: cmd, field: 'firstName', 'error')} required" >
	<label for="firstName">
        <g:message code="clinician.firstName.label" default="First Name" /> <span class="required-indicator">*</span>
	</label>
    <sec:isEditableUserField name="firstName">
	    <g:textField name="firstName" value="${cmd?.firstName}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="firstName">
        <g:field type="text" name="firstName" readonly="readonly" value="${cmd?.firstName}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
    </sec:isReadonlyUserField>
</div>

<div
	class="fieldcontain ${hasErrors(bean: cmd, field: 'lastName', 'error')} ">
	<label for="lastName">
        <g:message code="clinician.lastName.label" default="Last Name" /> <span class="required-indicator">*</span>
	</label>
    <sec:isEditableUserField name="lastName">
	    <g:textField name="lastName" value="${cmd?.lastName}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="lastName">
        <g:field type="text" name="lastName" readonly="readonly" value="${cmd?.lastName}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
    </sec:isReadonlyUserField>
</div>

<div class="fieldcontain ${hasErrors(bean: cmd, field: 'user.username', 'error')} required">
    <label for="username">
        <g:message code="clinician.username.label" default="Username" /> <span class="required-indicator">*</span>
    </label>
    <g:if test="${cmd?.id}">
        <g:field type="text" name="username" readonly="readonly" value="${cmd?.username}" data-tooltip="${message(code: 'tooltip.clinician.edit.username')}"/>
    </g:if>
    <g:else>
        <g:textField name="username" value="${cmd?.username}" data-tooltip="${message(code: 'tooltip.clinician.create.username')}"/>
    </g:else>
</div>

<g:if test="${!cmd.isSamlShadow}">
    <g:if test="${cmd?.cleartextPassword}">
    <div class="fieldcontain ${hasErrors(bean: cmd, field: 'cpr', 'error')} required" data-tooltip="${message(code: 'tooltip.clinician.create.cleartextPassword')}">

        <label for="cleartextPassword">
            <g:message code="clinician.cleartextPassword.label" default="Adgangskode" /> <span class="required-indicator">*</span>
        </label>
        <g:textField name="cleartextPassword" autocomplete="off" value="${cmd?.cleartextPassword}"/>
        <br />
    </div>
    </g:if>
    <g:else>
        <div class="fieldcontain ${hasErrors(bean: cmd, field: 'cpr', 'error')} required" data-tooltip="${message(code: 'tooltip.clinician.edit.password')}">

            <label for="password">
                <g:message code="clinician.password.label" default="Adgangskode" /> <span class="required-indicator">*</span>
            </label>
            <g:textField name="password" autocomplete="off" value="${message(code:"clinician.password.set-by-user")}" readonly="readonly"/>
            <br />
        </div>
    </g:else>
</g:if>

<div
	class="fieldcontain ${hasErrors(bean: cmd, field: 'phone', 'error')} ">
	<label for="phone">
        <g:message code="clinician.phone.label" default="Phone" />
	</label>
    <sec:isEditableUserField name="phone">
	    <g:textField name="phone" value="${cmd?.phone}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="phone">
        <g:field type="text" name="phone" readonly="readonly" value="${cmd?.phone}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
    </sec:isReadonlyUserField>
</div>

<div
	class="fieldcontain ${hasErrors(bean: cmd, field: 'mobilePhone', 'error')} ">
	<label for="mobilePhone">
        <g:message code="clinician.mobilePhone.label" default="Mobile Phone" />
	</label>
    <sec:isEditableUserField name="mobilePhone">
        <g:textField name="mobilePhone" value="${cmd?.mobilePhone}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="mobilePhone">
        <g:field type="text" name="mobilePhone" readonly="readonly" value="${cmd?.mobilePhone}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
    </sec:isReadonlyUserField>
</div>

<div
	class="fieldcontain ${hasErrors(bean: cmd, field: 'email', 'error')} ">
	<label for="email">
        <g:message code="clinician.email.label" default="Email" />
	</label>
    <sec:isEditableUserField name="email">
        <g:textField name="email" value="${cmd?.email}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="email">
        <g:field type="text" name="email" readonly="readonly" value="${cmd?.email}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
    </sec:isReadonlyUserField>
</div>

<g:if test="${grailsApplication.config.video.enabled}">
    <div class="fieldcontain ${hasErrors(bean: cmd, field: 'videoUser', 'error')} ">
        <label for="videoUser">
            <g:message code="clinician.video_user.label"/>
        </label>
        <sec:isEditableUserField name="videoUser">
            <g:textField name="videoUser" value="${cmd?.videoUser}" />
        </sec:isEditableUserField>
        <sec:isReadonlyUserField name="videoUser">
            <g:field type="text" name="videoUser" readonly="readonly" value="${cmd?.videoUser}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
        </sec:isReadonlyUserField>
    </div>

    <div  class="fieldcontain ${hasErrors(bean: cmd, field: 'videoPassword', 'error')} ">
        <label for="videoPassword">
            <g:message code="clinician.video_password.label"/>
        </label>
        <sec:isEditableUserField name="videoPassword">
            <g:textField name="videoPassword" value="${cmd?.videoPassword}" />
        </sec:isEditableUserField>
        <sec:isReadonlyUserField name="videoPassword">
            <g:field type="text" name="videoPassword" readonly="readonly" value="${cmd?.videoPassword}" data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}"/>
        </sec:isReadonlyUserField>
    </div>
</g:if>


<div class="fieldcontain ${hasErrors(bean: cmd, field: 'group', 'error')} required">
    <label for="groupIds">
        <g:message code="clinician.group.label" default="type" />
    </label>
    <sec:isEditableUserField name="patientGroupName">
        <g:select name="groupIds" from="${cmd.possiblePatientGroups}"
                  optionKey="id" multiple="multiple"
                  value="${cmd?.groupIds}"
                  data-tooltip="${message(code: 'tooltip.clinician.create.group')}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="patientGroupName">
        <g:select name="groupIds" from="${cmd.possiblePatientGroups}"
                  optionKey="id" multiple="multiple"
                  value="${cmd?.groupIds}"
                  disabled="disabled"
                  data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}" />
    </sec:isReadonlyUserField>
</div>

<div class="fieldcontain required">
    <label for="roleIds">
        <g:message code="clinician.roles.label" default="type" />
    </label>
    <sec:isEditableUserField name="roleName">
        <g:select name="roleIds" from="${cmd.possibleRoles}"
                  optionKey="id" multiple="multiple" optionValue="authority"
                  value="${cmd?.roleIds}"
                  data-tooltip="${message(code: 'clinician.roles.label')}" />
    </sec:isEditableUserField>
    <sec:isReadonlyUserField name="roleName">
        <g:select name="roleIds" from="${cmd.possibleRoles}"
                  optionKey="id" multiple="multiple" optionValue="authority"
                  value="${cmd?.roleIds}"
                  disabled="disabled"
                  data-tooltip="${message(code: 'tooltip.clinician.edit.claim')}" />
    </sec:isReadonlyUserField>
</div>

