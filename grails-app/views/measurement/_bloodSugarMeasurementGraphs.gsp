<%-- Please note: Requires the previous inclusion of _graphFunctions.gsp in the page, as it defines all the --%>
<%-- functionality needed by this file.                                                                     --%>

<%@ page import="grails.converters.JSON; org.opentele.server.model.NumericThreshold; org.opentele.server.model.types.MeasurementTypeName; org.opentele.server.model.types.MeasurementFilterType" %>
<g:if test="${measurement.type == MeasurementTypeName.BLOODSUGAR.name()}">
    <script type="text/javascript">

        var handleEmptySeries = function(series) { //JqPlot dislikes empty series, pushing 'null' fixes the problem.

            if(series[0].length === 0) {
                series[0].push(null);
            }

            if(series[1].length === 0) {
                series[1].push(null);
            }

            if(series[2].length === 0) {
                series[2].push(null);
            }

            return series;
        };

        var resetMeasurementDate = function(date) {
            var newDate = new Date(date);
            newDate.setYear(2014);
            newDate.setMonth(0);
            newDate.setDate(1);
            return newDate;
        };

        var formatDate = function(date) {
            return "" + date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
        };

        var prepareMeasurement = function (series, index) {
            var date = resetMeasurementDate(series[index][0]);
            series[index][0] = date.getTime();
        };

        var prepareMeasurementSeries = function(beforeMealSeries, afterMealSeries, unknownMealSeries) {
            var i;
            for (i = 0; i < beforeMealSeries.length; i++) {
                prepareMeasurement(beforeMealSeries, i);
            }

            for (i = 0; i < afterMealSeries.length; i++) {
                prepareMeasurement(afterMealSeries, i);
            }

            for (i = 0; i < unknownMealSeries.length; i++) {
                prepareMeasurement(unknownMealSeries, i);
            }

            return [beforeMealSeries, afterMealSeries, unknownMealSeries]
        };

        var ticksAsJSON = ${measurement.ticksX as JSON};

        var tickFormatter = function (format, val) {
            return $.jsDate.strftime(val, format);
        };

        var canvasOverlayObjects = [];

        // Mark target interval with a lightgray color
        canvasOverlayObjects.push({rectangle: {
            ymin: 4,
            ymax: 10,
            xminOffset: "0px",
            xmaxOffset: "0px",
            yminOffset: "0px",
            ymaxOffset: "0px",
            color: "rgba(211, 211, 211, 0.45)"
        }});

        <g:if test="${MeasurementFilterType.valueOf(params.filter) == MeasurementFilterType.CUSTOM} || ${MeasurementFilterType.valueOf(params.filter) == MeasurementFilterType.WEEK}">
        var numberOfTicks = ${measurement.ticksX.size()};
        if(numberOfTicks < 9) {
            for(var index = 0; index < ticksAsJSON.length-1; index=index+4) {
                var tick = ticksAsJSON[index];
                var tick1 = new Date(tick);
                var tick2 = new Date(tick);
                var tick3 = new Date(tick);
                tick1.setHours(6);
                tick1.setSeconds(1); // Hack to differentiate this date as a custom minor tick
                tick2.setHours(12);
                tick2.setSeconds(1);
                tick3.setHours(18);
                tick3.setSeconds(1);

                ticksAsJSON.splice(index+1, 0, tick1);
                ticksAsJSON.splice(index+2, 0, tick2);
                ticksAsJSON.splice(index+3, 0, tick3);

                canvasOverlayObjects.push({dashedVerticalLine: {
                    x: new Date(tick1).getTime(),
                    lineWidth: 0.5,
                    dashPattern: [2, 4],
                    color: 'rgb(66, 98, 144)',
                    shadow: false
                }});

                canvasOverlayObjects.push({dashedVerticalLine: {
                    x: new Date(tick2).getTime(),
                    lineWidth: 0.5,
                    dashPattern: [2, 4],
                    color: 'rgb(66, 98, 144)',
                    shadow: false
                }});

                canvasOverlayObjects.push({dashedVerticalLine: {
                    x: new Date(tick3).getTime(),
                    lineWidth: 0.5,
                    dashPattern: [2, 4],
                    color: 'rgb(66, 98, 144)',
                    shadow: false
                }});
            }

            // Set custom Tickformatter that dynamically switches formatter if ticks are not at zero seconds
            tickFormatter = function (format, val) {
                var testDate = new Date(val);
                if (testDate.getSeconds() == 0) {
                    return $.jsDate.strftime(val, format);
                } else {
                    return $.jsDate.strftime(val, '%H:%M');
                }
            };

            // Override setTick function to hide gridlines not at zero seconds by setting them to minorTick
            $.jqplot.CanvasAxisTickRenderer.prototype.setTick = function(value, axisName, isMinor) {
                this.value = value;
                var testDate = new Date(value);
                this.isMinorTick = testDate.getSeconds() != 0;
                return this;
            };
        }
        </g:if>

        var series = ${measurement.series as JSON};
        var id = 0;
        series.forEach(function(aSeries) {
            aSeries.forEach(function(aMeasurement) {


                aMeasurement.push(id);
                id++;
            });
        });

        var clone = function(obj) {
            return JSON.parse(JSON.stringify(obj));
        };

        var seriesWithDatesReset = clone(series);
        seriesWithDatesReset = handleEmptySeries(prepareMeasurementSeries(seriesWithDatesReset[0], seriesWithDatesReset[1], seriesWithDatesReset[2]));

        window.drawGraph({
            containerId: '${measurement.type}-${patient.id}',
            title: '${title != null ? title : message(code: 'patient.graph.of', args: [message(code: "graph.legend.BLOODSUGAR"), patient.name.encodeAsHTML()])}',
            series: handleEmptySeries(series),

            seriesColors: ${measurement.seriesColors as JSON},
            showLegend: true,

            canvasOverlayObjects: canvasOverlayObjects,
            tickFormatter: tickFormatter,
            formatStringX: '${message(code:"graph.label.x.format.BLOODSUGAR")}',
            ticksX: ticksAsJSON,

            minimumY: ${measurement.minY},
            maximumY: ${measurement.maxY},
            labelY: '${message(code:"graph.label.y.BLOODSUGAR")}',
            ticksY: ${measurement.ticksY.collect { [it.value, it.text] } as JSON },
            alarmValues: ${measurement.alertValues as JSON},
            warningValues: ${measurement.warningValues as JSON},

            <g:if test="${patientIdForFullScreen != null}">
            doubleClickFunction: function (ev, seriesIndex, pointIndex, data) {
                document.location = '${createLink(mapping:"patientMeasurementGraph", params:[patientId: patientIdForFullScreen, measurementType: measurement.type])}' + window.location.search;
            },
            </g:if>
            singleClickFunction: function(ev, seriesIndex, pointIndex, data) {
                window.location.href = '${createLink(controller:"measurement", action:"show")}/' + ${measurement.seriesIds}[seriesIndex][pointIndex];
            }
        }, function(plot) {
            drawStandardGraph(plot);
        });

        var drawStandardGraph = function(plot) {

            var canvasOverlayObjects = [];

            // Mark target interval with a lightgray color
            canvasOverlayObjects.push({rectangle: {
                ymin: 4,
                ymax: 10,
                xminOffset: "0px",
                xmaxOffset: "0px",
                yminOffset: "0px",
                ymaxOffset: "0px",
                color: "rgba(211, 211, 211, 0.45)"
            }});

            canvasOverlayObjects.push({verticalLine: {
                x: new Date("Jan 01, 2014 08:00:00").getTime(),
                lineWidth: 3.0,
                dashPattern: [2, 4],
                color: 'rgb(66, 98, 144)',
                shadow: false
            }});

            canvasOverlayObjects.push({verticalLine: {
                x: new Date("Jan 01, 2014 12:00:00").getTime(),
                lineWidth: 3.0,
                dashPattern: [2, 4],
                color: 'rgb(66, 98, 144)',
                shadow: false
            }});

            canvasOverlayObjects.push({verticalLine: {
                x: new Date("Jan 01, 2014 16:00:00").getTime(),
                lineWidth: 3.0,
                dashPattern: [2, 4],
                color: 'rgb(66, 98, 144)',
                shadow: false
            }});

            canvasOverlayObjects.push({verticalLine: {
                x: new Date("Jan 01, 2014 20:00:00").getTime(),
                lineWidth: 3.0,
                dashPattern: [2, 4],
                color: 'rgb(66, 98, 144)',
                shadow: false
            }});

            var standardDayContainerId = '${MeasurementTypeName.BLOODSUGAR.name()}-average-day-${patient.id}';

            window.drawGraph({
                containerId: standardDayContainerId,
                title: '${title != null ? title : message(code: 'patient.graph.of', args: [message(code: "graph.legend.BLOODSUGAR_AVERAGE_DAY"), patient.name.encodeAsHTML()])}',
                series: seriesWithDatesReset,
                seriesColors: ${measurement.seriesColors as JSON},
                showLegend: true,

                canvasOverlayObjects: canvasOverlayObjects,
                formatStringX: '${message(code:"graph.label.x.format.BLOODSUGAR_AVERAGE_DAY")}',
                ticksX: [new Date("Jan 01, 2014 00:00:00"),
                    new Date("Jan 01, 2014 01:00:00"),
                    new Date("Jan 01, 2014 02:00:00"),
                    new Date("Jan 01, 2014 03:00:00"),
                    new Date("Jan 01, 2014 04:00:00"),
                    new Date("Jan 01, 2014 05:00:00"),
                    new Date("Jan 01, 2014 06:00:00"),
                    new Date("Jan 01, 2014 07:00:00"),
                    new Date("Jan 01, 2014 08:00:00"),
                    new Date("Jan 01, 2014 09:00:00"),
                    new Date("Jan 01, 2014 10:00:00"),
                    new Date("Jan 01, 2014 11:00:00"),
                    new Date("Jan 01, 2014 12:00:00"),
                    new Date("Jan 01, 2014 13:00:00"),
                    new Date("Jan 01, 2014 14:00:00"),
                    new Date("Jan 01, 2014 15:00:00"),
                    new Date("Jan 01, 2014 16:00:00"),
                    new Date("Jan 01, 2014 17:00:00"),
                    new Date("Jan 01, 2014 18:00:00"),
                    new Date("Jan 01, 2014 19:00:00"),
                    new Date("Jan 01, 2014 20:00:00"),
                    new Date("Jan 01, 2014 21:00:00"),
                    new Date("Jan 01, 2014 22:00:00"),
                    new Date("Jan 01, 2014 23:00:00"),
                    new Date("Jan 01, 2014 23:59:59")],

                minimumY: ${measurement.minY},
                maximumY: ${measurement.maxY},
                labelY: '${message(code:"graph.label.y.BLOODSUGAR")}',
                ticksY: ${measurement.ticksY.collect { [it.value, it.text] } as JSON },
                alarmValues: ${measurement.alertValues as JSON},
                warningValues: ${measurement.warningValues as JSON},

                <g:if test="${patientIdForFullScreen != null}">
                doubleClickFunction: function (ev, seriesIndex, pointIndex, data) {
                    document.location = '${createLink(mapping:"patientMeasurementGraph", params:[patientId: patientIdForFullScreen, measurementType: measurement.type])}' + window.location.search;
                },
                </g:if>
                singleClickFunction: function (ev, seriesIndex, pointIndex, data) {
                    window.location.href = '${createLink(controller:"measurement", action:"show")}/' + ${measurement.seriesIds}[seriesIndex][pointIndex];
                },
                highlighterFormatString: '<table class="jqplot-highlighter-tooltip"><tr><td>%3$s, %5$s</td></tr>%4$s</table>'
            }, function() {
                addMouseOverHighlightEvent(standardDayContainerId, plot);
            });

            var findCorrespondingPointIndex = function (plot, seriesIndex, point) {
                var standardDayPointId = point[5];
                var series = plot.series[seriesIndex].data;
                var counter = 0;
                series.some(function (point) {
                    var pointId = point[5];
                    if (pointId === standardDayPointId) {
                        return true;
                    }
                    ++counter;
                });
                return counter;
            };

            var hexToRgb = function (hex, opacity) {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                return 'rgba(' +
                        parseInt(result[1], 16) + ',' +
                        parseInt(result[2], 16) + ',' +
                        parseInt(result[3], 16) + ',' +
                        opacity + ')';
            };

            var highlightPoint = function (plot, seriesIndex, pointIndex) {
                var series = plot.series[seriesIndex];
                var color = hexToRgb(series.color, 0.6);
                var x = plot.axes.xaxis.series_u2p(series.data[pointIndex][0]);
                var y = plot.axes.yaxis.series_u2p(series.data[pointIndex][1]);
                var radius = 6; // radius
                var end = 2 * Math.PI;
                var bloodSugarGraph = $(plot.targetId);
                var drawingCanvas = bloodSugarGraph.find('.jqplot-highlight-canvas')[0];
                var context = drawingCanvas.getContext('2d');
                context.clearRect(0, 0, drawingCanvas.width, drawingCanvas.height);
                context.strokeStyle = 'rgba(0,0,0,0)';
                context.fillStyle = color;
                context.beginPath();
                context.arc(x, y, radius, 0, end, true);
                context.closePath();
                context.stroke();
                context.fill();
            };

            var addMouseOverHighlightEvent = function (containerId, plot) {
                var MOUSE_OVER_EVENT = 'jqplotDataMouseOver';
                var container = $('#' + containerId);
                container.bind(MOUSE_OVER_EVENT, function (evt, seriesIndex, pointIndex, data) {
                    var otherPointIndex = findCorrespondingPointIndex(plot, seriesIndex, data);
                    highlightPoint(plot, seriesIndex, otherPointIndex);
                });
            };
        };
    </script>
</g:if>