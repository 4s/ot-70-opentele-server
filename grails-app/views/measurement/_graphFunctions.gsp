<g:javascript src="jqplot/jquery.jqplot.js"/>
<g:javascript src="jqplot/plugins/jqplot.canvasAxisLabelRenderer.js" />
<g:javascript src="jqplot/plugins/jqplot.canvasAxisTickRenderer.js" />
<g:javascript src="jqplot/plugins/jqplot.canvasTextRenderer.js" />
<g:javascript src="jqplot/plugins/jqplot.canvasOverlay108.js" /> <!-- Force use of newer version canvas overlay with rectangle support -->
<g:javascript src="jqplot/plugins/jqplot.dateAxisRenderer.js" />
<g:javascript src="jqplot/plugins/jqplot.cursor.js" />
<g:javascript src="jqplot/plugins/jqplot.highlighter.js" />

<!--[if lt IE 9]>
<g:javascript src="jqplot/excanvas.js"/>
<![endif]-->

<script type="text/javascript">
    window.drawGraph = function(options, callback) {
        $(function() {

            $.jqplot.postDrawHooks.push(function() {
                $(".jqplot-overlayCanvas-canvas").css('z-index', '0'); //send overlay canvas to back
                $(".jqplot-series-canvas").css('z-index', '1'); //send series canvas to front
                $(".jqplot-table-legend").css('z-index', '2'); //send legend to front
                $(".jqplot-highlighter-tooltip").css('z-index', '3'); //make sure the tooltip is over the series
                $(".jqplot-event-canvas").css('z-index', '5'); //must be on the very top since it is responsible for event catching and propagation
            });

            var containerId = options['containerId'];
            var title = options['title'];
            var series = options['series'];

            var canvasOverlayObjects = options['canvasOverlayObjects'];
            if(canvasOverlayObjects == undefined) {
                canvasOverlayObjects = Array();
            }

            var tickFormatter = options['tickFormatter'];
            var formatStringX = options['formatStringX'];
            var ticksX = options['ticksX'];

            var minimumY = options['minimumY'];
            var maximumY = options['maximumY'];
            var labelY = options['labelY'];
            var ticksY = options['ticksY'];

            <g:if test="${request.showThresholds}">
            var alarmValues = options['alarmValues'];
            var warningValues = options['warningValues'];
            </g:if>
            <g:else>
            var alarmValues = [];
            var warningValues = [];
            </g:else>

            var measurementType = options['measurementType'];
            var seriesColors = options['seriesColors'];
            var highlighterFormatString = options['highlighterFormatString'];

            var singleClickFunction = options['singleClickFunction'];
            var doubleClickFunction = options['doubleClickFunction'];

            var container = $('#' + containerId);
            var showLegend = options['showLegend'];
            <g:if test="${request.showThresholds}">
            container.bind('jqplotDataClick', singleClickFunction);
            if (doubleClickFunction) {
                container.bind('jqplotDblClick', doubleClickFunction);
            }
            </g:if>
            <g:if test="${request.showThresholds} || ${request.showHighlighterFormatString}">
            if(!highlighterFormatString) {
                <g:if test="${request.graphForPatient}">
                highlighterFormatString = '<div>%1$s %3$s, %5$s</div>';
                </g:if>
                <g:else>
                for(var i = 0; i < series.length; i++) {
                    var aSeries = series[i];
                    for(var j = 0; j < aSeries.length; j++) {
                        var aMeasurement = aSeries[j];
                        if(aMeasurement !== null) {
                            var ackNote = aMeasurement[3];

                            if(ackNote != '') {
                                aMeasurement[3] = '<tr><td>${message(code:'patient.acknowledge.note')}:</td></tr><tr><td nowrap="wrap">' + ackNote + '</td></tr>';
                            }
                        }
                    }
                }
                highlighterFormatString = '<table class="jqplot-highlighter-tooltip"><tr><td>%1$s %3$s, %5$s</td></tr>%4$s</table>';
                </g:else>
            }
            </g:if>
            <g:else>
            highlighterFormatString = '<div>%1$s %3$s, %5$s</div>';
            </g:else>

            var seriesConfig;
            switch(measurementType) {
                case 'BLOOD_PRESSURE':
                    if (seriesColors) {
                        seriesConfig = [];
                        for(var i = 0; i < seriesColors.length; i++) {
                            seriesConfig[i] = {
                                showLine: true,
                                color: seriesColors[i],
                                markerOptions: {size: 5}
                            }

                            seriesConfig[2] = { // Third series is for heart rate (pulse) in blood pressure graph
                                showLine: false,
                                color: seriesColors[i],
                                markerOptions: { style: "x", size: 7}
                            };
                        }

                        seriesConfig[0].label = "${message(code: 'patient.overview.measurements.SYSTOLIC_BLOOD_PRESSURE')}";
                        seriesConfig[1].label = "${message(code: 'patient.overview.measurements.DIASTOLIC_BLOOD_PRESSURE')}";
                        seriesConfig[2].label = "${message(code: 'patient.overview.table.label.PULSE')}";
                    }
                    break;
                case 'SYSTOLIC_BLOOD_PRESSURE':
                    if (seriesColors) {
                        seriesConfig = [{
                            showLine: true,
                            color: seriesColors[0],
                            markerOptions: {size: 5}
                        }];

                        seriesConfig[0].label = "${message(code: 'patient.overview.measurements.SYSTOLIC_BLOOD_PRESSURE')}";
                    }

                    // Mark target systolic interval with a lightgray color
                    canvasOverlayObjects.push({rectangle: {
                        ymin: 90,
                        ymax: 130,
                        xminOffset: "0px",
                        xmaxOffset: "0px",
                        yminOffset: "0px",
                        ymaxOffset: "0px",
                        color: "rgba(211, 211, 211, 0.45)"
                    }});

                    break;
                case 'DIASTOLIC_BLOOD_PRESSURE':
                    if (seriesColors) {
                        seriesConfig = [{
                            showLine: true,
                            color: seriesColors[0],
                            markerOptions: {size: 5}
                        }];

                        seriesConfig[0].label = "${message(code: 'patient.overview.measurements.DIASTOLIC_BLOOD_PRESSURE')}";
                    }

                    // Mark target diastolic interval with a lightgray color
                    canvasOverlayObjects.push({rectangle: {
                        ymin: 60,
                        ymax: 80,
                        xminOffset: "0px",
                        xmaxOffset: "0px",
                        yminOffset: "0px",
                        ymaxOffset: "0px",
                        color: "rgba(211, 211, 211, 0.45)"
                    }});

                    break;
                case 'PULSE':
                    if (seriesColors) {
                        seriesConfig = [{
                            showLine: true,
                            color: seriesColors[0],
                            markerOptions: { style: "x", size: 7}
                        }];

                        seriesConfig[0].label = "${message(code: 'patient.overview.table.label.PULSE')}";
                    }

                    // Mark target pulse interval with a lightgray color
                    canvasOverlayObjects.push({rectangle: {
                        ymin: 60,
                        ymax: 100,
                        xminOffset: "0px",
                        xmaxOffset: "0px",
                        yminOffset: "0px",
                        ymaxOffset: "0px",
                        color: "rgba(211, 211, 211, 0.45)"
                    }});

                    break;
                case 'CONTINUOUS_BLOOD_SUGAR_MEASUREMENT':
                    seriesConfig = [
                        {
                            label: "${message(code: 'graph.cgm.legend.continuousBloodSugarMeasurementSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.hypoAlarmEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.hyperAlarmEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.impendingHypoAlarmEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.impendingHyperAlarmEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.coulometerReadingEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.insulinEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.exerciseEventSeries.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.stateOfHealthEvent.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        },
                        {
                            label: "${message(code: 'graph.cgm.legend.mealEvent.legend')}",
                            showLine: false,
                            markerOptions: {size: 5}
                        }
                    ]
                    break;
                default:
                    if (seriesColors) {
                        seriesConfig = [];
                        for(var i = 0; i < seriesColors.length; i++) {
                            seriesConfig[i] = {
                                showLine: false,
                                color: seriesColors[i],
                                markerOptions: {size: 5}
                            }
                        }

                        seriesConfig[0].label = "${message(code: 'patient.overview.bloodsugar.beforemeal')}";
                        if (seriesColors.length > 1) {
                            seriesConfig[1].label = "${message(code: 'patient.overview.bloodsugar.aftermeal')}";
                        }
                        if (seriesColors.length > 2) {
                            seriesConfig[2].label = "${message(code: 'patient.overview.bloodsugar.unknown')}";
                        }
                    }
                    break;
            }

            var graph = $.jqplot (containerId, series, {
                title: title,
                gridPadding: { left: 40 },
                series: seriesConfig,

                axes: {
                    xaxis: {
                        pad: 1.2,
                        renderer: $.jqplot.DateAxisRenderer,
                        rendererOptions: {
                            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                            tickOptions: {
                                formatter: tickFormatter
                            }
                        },
                        ticks: ticksX,
                        tickOptions: {
                            formatString: formatStringX,
                            angle: -45
                        }
                    },
                    yaxis: {
                        min: minimumY,
                        max: maximumY,
                        label: labelY,
                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                        ticks: ticksY
                    }
                },
                canvasOverlay: {
                    show: true,
                    objects: canvasOverlayObjects
                },
                legend:{
                    renderer: $.jqplot.EnhancedLegendRenderer,
                    show: showLegend,
                    location: 'nw'
                },
                highlighter: {
                    show: true,
                    sizeAdjust: 7.5,
                    tooltipOffset: 3,
                    fadeTooltip:true,
                    tooltipLocation: "sw",
                    yvalues: 5,
                    formatString: highlighterFormatString
                },
                cursor: {
                    show: false
                }
            });

            function drawLine(color, y) {
                var canvas = $('#' + containerId + '>.jqplot-series-canvas')[0];
                if (canvas) {
                    var context = canvas.getContext("2d");
                    context.save();
                    context.strokeStyle = color;

                    var distanceFromLowerEdge = y - minimumY;
                    var canvasY = canvas.height - (distanceFromLowerEdge * canvas.height)/(maximumY - minimumY);
                    context.beginPath();
                    context.moveTo(0, canvasY);
                    context.lineTo(canvas.width, canvasY);
                    context.closePath();
                    context.stroke();

                    context.restore();
                }
            }

            function drawThresholdIndicators() {
                var i;
                for (i = 0; i<alarmValues.length; i++) {
                    drawLine('red', alarmValues[i]);
                }
                for (i = 0; i<warningValues.length; i++) {
                    drawLine('yellow', warningValues[i]);
                }
            }
            // A bit hacky... if the div for the graph has style "display:none" when the graph is defined, JQPlot
            // does not plot the diagram. When the diagram is later shown, the code showing the graph must then
            // "trigger" the 'visibilityChanged' event to allow us to replot the graph.
            container.bind('visibilityChanged', function() {
                graph.replot();
                drawThresholdIndicators();
            });

            drawThresholdIndicators();

            if (callback !== undefined) {
                callback(graph);
            }
        });
    }
</script>
