package org.opentele.server.util


class PasswordUtil {

    static Closure getPasswordValidator() {

        return { password ->
            if (password) {
                if (password.size() < 8) {
                    return "validator.too.short"
                }
                def onlyDigitsAndAlpha = password =~ /^[\p{Graph}]{8,}$/
                if (!onlyDigitsAndAlpha) {
                    return "validator.only.alphanumeric"
                }
                def atLeastOneDigit = password =~ /\p{Digit}+/
                if (!atLeastOneDigit.size()) {
                    return "validator.missing.digits"
                }
                def atLeastOneAlpha = password =~ /\p{Alpha}+/
                if (!atLeastOneAlpha.size()) {
                    return "validator.missing.alphas"
                }
            }
            return true
        }
    }

    static Closure getCleartextPasswordValidator() {

        return { password ->
            if (password) {
                if (password.size() < 8) {
                    return "validator.cleartext.too.short"
                }
                def onlyDigitsAndAlpha = password =~ /^[\p{Graph}]{8,}$/
                if (!onlyDigitsAndAlpha) {
                    return "validator.cleartext.only.alphanumeric"
                }
                def atLeastOneDigit = password =~ /\p{Digit}+/
                if (!atLeastOneDigit.size()) {
                    return "validator.cleartext.missing.digits"
                }
                def atLeastOneAlpha = password =~ /\p{Alpha}+/
                if (!atLeastOneAlpha.size()) {
                    return "validator.cleartext.missing.alphas"
                }
            }
            return true
        }
    }

    static Closure getSimplePasswordValidator() {
        return { password ->
            if (password) {
                def match = password =~ /(?=^\p{Graph}{8,}$)(?=.*\p{Alpha}+)(?=.*\p{Digit}+)/
                if (!match) return "validator.only.alphanumeric"
            }
            return true
        }
    }
}
